package com.configuration.util;

import com.netflix.appinfo.AmazonInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.commons.util.InetUtils;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
@EnableConfigServer
@EnableEurekaClient
public class ConfigServiceApplication {
	@Value("${server.port}") private int port;
	
	public static void main(String[] args) {
		SpringApplication.run(ConfigServiceApplication.class, args);
	}
	
	@Bean
	@Profile({"production"})
	public EurekaInstanceConfigBean eurekaInstanceConfig(InetUtils inetUtils){
		EurekaInstanceConfigBean bean = new EurekaInstanceConfigBean(inetUtils);
		AmazonInfo dataCenterInfo = AmazonInfo.Builder.newBuilder().autoBuild("eureka");
		bean.setHostname(dataCenterInfo.get(AmazonInfo.MetaDataKey.publicHostname));
		bean.setIpAddress(dataCenterInfo.get(AmazonInfo.MetaDataKey.publicIpv4));
		bean.setNonSecurePort(port);
		bean.setDataCenterInfo(dataCenterInfo);
		return bean;
	}
}
